{-# LANGUAGE DeriveDataTypeable #-}


module Theme (
 Triple, foreground, background, border, fg, bg,
 Theme, fontspec, barheight, normal, focused, highlight, urgent, hidden,
 -- Twilight themes
 twilightLight, twilightDark, twilightDarkWithLightBorders,
 -- Tango theme
 tango,
 -- Tomorrow themes
 tomorrow, tomorrowNightEighties
 ) where


import Data.Data
import Data.Typeable


type Color = String
type Font = String

data Triple = Triple
  { foreground :: Color
  , background :: Color
  , border :: Color
  } deriving (Data, Show, Typeable)

fg = foreground
bg = background

data Theme = Theme
  { fontspec :: Font
  , barheight :: Int
  , normal :: Triple
  , focused :: Triple
  , highlight :: Triple
  , urgent :: Triple
  , hidden :: Triple
  } deriving (Data, Show, Typeable)


font = "xft:CMU Typewriter Text:size=11"
height = 32


twilightLight = Theme
  { fontspec = font
  , barheight = height
  , normal = Triple "#505050" "#ffffff" "#cc8c8c8"
  , focused = Triple "#505050" "#eaeaea" "#eaeaea"
  , highlight = Triple "#d2ad00" "#faf7e7" "#faf7e7"
  , hidden = Triple "#a49da5" "#f7f7f7" "#f7f7f7"
  , urgent = Triple "#d15120" "#fdf2ed" "#fdf2ed"
  }

twilightDark = Theme
  { fontspec = font
  , barheight = height
  , normal = Triple "#dcdddd" "#181d23" "#181d23"
  , focused = Triple "#00959e" "#1b333e" "#1b333e"
  , highlight = Triple "#00959e" "#1b333e" "#1b333e"
  , hidden = Triple "#313c4d" "#181d23" "#181d23"
  , urgent = Triple "#deae3e" "#2a2921" "#2a2921"
  }

twilightDarkWithLightBorders = Theme
  { fontspec = font
  , barheight = height
  , normal = Triple "#dcdddd" "#181d23" "#eaeaea"
  , focused = Triple "#ffffff" "#313c4d" "#313c4d"
  , highlight = Triple "#00959e" "#1b333e" "#1b333e"
  , hidden = Triple "#716d73" "#181d23" "#181d23"
  , urgent = Triple "#deae3e" "#2a2921" "#2a2921"
  }

tango = Theme
  { fontspec = font
  , barheight = height
  , normal = Triple "#eeeeec" "#2e3436" "#555753"
  , focused = Triple "#ffffff" "#555753" "#555753"
  , highlight = Triple "#8ae234" "#2e3436" "#2e3436"
  , hidden = Triple "#888a85" "#2e3436" "#2e3436"
  , urgent = Triple "#2e3436" "#fcaf3e" "#fcaf3e"
  }

tomorrow = Theme
  { fontspec = font
  , barheight = height
  , normal = Triple "#ffffff" "#4d4d4c" "#4d4d4c"
  , focused = Triple "#323232" "#8e908c" "#8e908c"
  , highlight = Triple "#eab700" "#4d4d4c" "#4d4d4c"
  , hidden = Triple "#8e908c" "#4d4d4c" "#4d4d4c"
  , urgent = Triple "#4d4d4c" "#eab700" "#eab700"
  }

tomorrowNightEighties = Theme
  { fontspec = font
  , barheight = height
  , normal = Triple "#cccccc" "#2d2d2d" "#2d2d2d"
  , focused = Triple "#cccccc" "#515151" "#515151"
  , highlight = Triple "#ffcc66" "#2d2d2d" "#2d2d2d"
  , hidden = Triple "#515151" "#2d2d2d" "#2d2d2d"
  , urgent = Triple "#2d2d2d" "#ffcc66" "#ffcc66"
  }
