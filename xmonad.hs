-- Imports --
-------------

-- Standard modules
import qualified Data.Map as Map
import Data.Ratio
import System.Environment
import System.Exit
import System.IO
import Text.StringTemplate
import Text.StringTemplate.GenericStandard

-- XMonad modules
import XMonad
import XMonad.Actions.FocusNth
import XMonad.Hooks.DynamicLog hiding (xmobarPP, xmobar, dzenPP, dzen)
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.UrgencyHook
import XMonad.Layout.Grid
import XMonad.Layout.IM
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.Named
import XMonad.Layout.PerWorkspace
import XMonad.Layout.Tabbed
import XMonad.ManageHook
import qualified XMonad.StackSet as Stack
import XMonad.Prompt
import XMonad.Prompt.Shell
import XMonad.Util.EZConfig
import XMonad.Util.Run

-- User-defined modules
import Theme


-- Prompts --
-------------

promptConfig = def
  { font = fontspec myTheme
  , height = fromIntegral . barheight $ myTheme
  , fgColor = fg . normal $ myTheme
  , bgColor = bg . normal $ myTheme
  , fgHLight = fg . highlight $ myTheme
  , bgHLight = bg . highlight $ myTheme
  , borderColor = bg . normal $ myTheme
  , position = Bottom
  }


-- Status bar --
----------------

compileWithTheme theme templateFile outputFile = do
    templateString <- readFile templateFile
    let template = newSTMP templateString :: StringTemplate String
    let output = toString $ setAttribute "theme" theme template
    writeFile outputFile output

toggleStrutsKey XConfig{modMask = modm} = (modm, xK_b)

makePP colorf = def
  { ppCurrent = colorf (fg . focused $ myTheme) (bg . focused $ myTheme) . dpad
  , ppVisible = colorf (fg . focused $ myTheme) (bg . normal $ myTheme) . dpad
  , ppHidden = colorf (fg . normal $ myTheme) (bg . hidden $ myTheme) . dpad
  , ppHiddenNoWindows = colorf (fg . hidden $ myTheme) (bg . normal $ myTheme) . dpad
  , ppUrgent = colorf (fg . urgent $ myTheme) (bg . urgent $ myTheme) . dpad
  , ppLayout = colorf (fg . hidden $ myTheme) (bg . normal $ myTheme) . dpad . wrap "[" "]"
  , ppTitle = const ""
  , ppWsSep = ""
  , ppSep = ""
  }
  where dpad = pad . pad

xmobarPP = makePP xmobarColor
xmobar = statusBar "xmobar" xmobarPP toggleStrutsKey


-- Layouts --
-------------

tabbedConfig = def
  { activeColor = bg . focused $ myTheme
  , activeTextColor = fg . focused $ myTheme
  , activeBorderColor = border . focused $ myTheme
  , inactiveColor = bg . normal $ myTheme
  , inactiveTextColor = fg . normal $ myTheme
  , inactiveBorderColor = bg . normal $ myTheme
  , urgentColor = bg . urgent $ myTheme
  , urgentTextColor = fg . urgent $ myTheme
  , urgentBorderColor = border . urgent $ myTheme
  , decoHeight = fromIntegral . barheight $ myTheme
  , fontName = fontspec $ myTheme
  }

layoutGrid = named "G" $ Grid
layoutMirror = named "H" $ Mirror layoutTall
layoutTabbed = named "T" $ tabbedAlways shrinkText tabbedConfig
layoutTall = named "V" $ Tall 1 (1/2) (1/2)

myLayoutHook = mkToggle (single FULL)
  $ layoutGrid ||| layoutTall ||| layoutMirror ||| layoutTabbed

fullscreen = do
    sendMessage ToggleStruts
    sendMessage (Toggle FULL)


-- Managing --
--------------

myManageHook = composeAll
  [ className =? "gl" --> doFloat
  , className =? "mpv" --> doFloat
  , className =? "Gimp" --> doFloat
  , className =? "Plugin-container" --> doFloat
  , className =? "Gnuplot_qt" --> doFloat
  , isFullscreen --> doFullFloat
  ]


-- Keybindings --
-----------------

myKeys c = mkKeymap c $
  -- XMonad recompilation and exit
  [ ("M-<Esc>" , spawn "xmonad --recompile; xmonad --restart")
  , ("M-S-<Esc>" , io exitSuccess)
  -- Suspend and hibernate
  , ("M-S-s" , spawn "sudo systemctl suspend")
  , ("M-S-h" , spawn "sudo systemctl hibernate")
  -- Essential applications to spawn
  , ("M-t" , spawn $ XMonad.terminal c)
  , ("M-n" , spawn $ (XMonad.terminal c) ++ " -e ncmpcpp")
  -- , ("M-n" , spawn "dbus-launch nautilus")
  , ("M-m" , spawn "emacsclient -c")
  , ("M-e" , spawn "evince")
  -- Shell prompt
  , ("M-r" , shellPrompt promptConfig)
  -- Brightness control
  , ("<XF86MonBrightnessDown>" , spawn "xbacklight - 10")
  , ("<XF86MonBrightnessUp>" , spawn "xbacklight + 10")
  -- Volume control
  , ("<XF86AudioRaiseVolume>" , spawn "amixer -M -D pulse set Master 5%+")
  , ("<XF86AudioLowerVolume>" , spawn "amixer -M -D pulse set Master 5%-")
  , ("<XF86AudioMute>" , spawn "amixer -D pulse set Master toggle")
  , ("M-<F11>" , spawn "amixer -M -D pulse set Master 5%+")
  , ("M-<F10>" , spawn "amixer -M -D pulse set Master 5%-")
  , ("M-<F8>" , spawn "amixer -D pulse set Master toggle")
  -- Music player control
  , ("M-<F12>" , spawn "ncmpcpp prev")
  , ("M-<Pause>" , spawn "ncmpcpp toggle")
  , ("M-<Scroll_lock>" , spawn "ncmpcpp next")
  -- Rotate through the available layouts algorithms
  , ("M-<Space>" , sendMessage NextLayout)
  -- Client management
  , ("M-q" , kill)
  , ("M-w" , withFocused $ windows . Stack.sink)
  , ("M-f" , fullscreen)
  , ("M-j" , windows Stack.focusDown)
  , ("M-k" , windows Stack.focusUp)
  , ("M-S-j" , windows Stack.swapDown)
  , ("M-S-k" , windows Stack.swapUp)
  ]
  ++
  -- Workspace switching
  [ ("M-<F" .+ k ++ ">", windows $ Stack.greedyView w)
  | (k, w) <- zip ['1' .. '9'] (XMonad.workspaces c) ]
  ++
  -- Workspace shifting
  [ ("M-S-<F" .+ k ++ ">", windows $ Stack.shift w)
  | (k, w) <- zip ['1' .. '9'] (XMonad.workspaces c) ]
  ++
  -- Switch to window
  [ ("M-" .+ k , focusNth n)
  | (k, n) <- zip ['1' .. '9'] [0 .. ] ]
  where
    xs .+ x = xs ++ [x]


-- Running --
-------------

myTheme = twilightDark
myWorkspaces = ["ω", "ξ", "α", "β", "γ"]

xmonadConfig = ewmh $ def
  { modMask = mod4Mask
  , workspaces = myWorkspaces
  , keys = myKeys
  , layoutHook = myLayoutHook
  , manageHook = myManageHook
  , handleEventHook = handleEventHook defaultConfig <+> fullscreenEventHook
  , normalBorderColor = border . normal $ myTheme
  , focusedBorderColor = border . focused $ myTheme
  }

main = do
  let templateFile = "/home/me/.xmonad/xmobar.d/xmobarrc"
  let outputFile = "/home/me/.xmobarrc"
  compileWithTheme myTheme templateFile outputFile
  xmobar xmonadConfig >>= xmonad . withUrgencyHook NoUrgencyHook
