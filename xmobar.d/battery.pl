#!/usr/bin/env perl

use strict;
use warnings;

my $status    = `acpi`;
my $indicator = '';

if ($status =~ /^Battery \d: (\w+), (\d+)%, (.+) (.*)$/) {
  my ($mode, $percentage, $time) = ($1, $2, $3);
  my ($mode_indicator, $time_indicator);

  $mode_indicator = '-' if ($mode eq 'Discharging');
  $mode_indicator = '+' if ($mode eq 'Charging');

  $time =~ /(\d{2}):(\d{2}):\d{2}/;
  $time_indicator = "$1:$2";

  $indicator = " $mode_indicator$time_indicator ";
}

print $indicator;
