#!/bin/sh

num=`cd /home/me/mail/ \
  && find -path './cur/*' -name '*:2,' -print \
  | wc -l`;

if test $num != 0; then
    echo " $num GMail "
fi
