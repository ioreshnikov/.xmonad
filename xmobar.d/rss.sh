#!/bin/sh

num=`cd /home/me/mail/RSS \
  && find . -name '*:2,' -print \
  | wc -l`;

if test $num != 0; then
    echo " $num RSS "
fi
